let project = "dist"; //папка с готовым проектом
let source = "src"; //рабочая папка

let path = {
  build: {
    html: project + "/",
    css: project + "/css/",
    js: project + "/js/",
    img: project + "/img/",
    fonts: project + "/fonts/",
  },
  src: {
    html: [source + "/*.html", "!" + source + "/_*.html"], //исключаем из дист все файлы html, которые начинаются на _
    css: source + "/scss/style.scss", //указали только этот файл исходник. Другие файлы в дист копироваться не будут
    js: source + "/js/script.js",
    img: source + "/img/**/*.{jpg,png,svg,gif,ico,webp}",
    fonts: source + "/fonts/*.ttf",
  },
  watch: {
    html: source + "/**/*.html",
    css: source + "/scss/**/*.scss",
    js: source + "/js/**/*.js",
    img: source + "/img/**/*.{jpg,png,svg,gif,ico,webp}",
    fonts: source + "/fonts/*.ttf",
  },
  clean: "./" + project + "/",
};

//Подключаем плагины, после их установки

let { src, dest } = require("gulp");
gulp = require("gulp");
browsersync = require("browser-sync").create();
fileinclude = require("gulp-file-include");
del = require("del");
const sass = require("gulp-sass")(require("sass"));
autoprefixer = require("gulp-autoprefixer");
group_media = require("gulp-group-css-media-queries");
clean_css = require("gulp-clean-css");
rename = require("gulp-rename");
uglify = require("gulp-uglify-es").default;
imagemin = require("gulp-imagemin");
webp = require("gulp-webp");
webphtml = require("gulp-webp-html");
webpcss = require("gulp-webpcss");
ttf2woff2 = require("gulp-ttf2woff2");

function browserSync() {
  browsersync.init({
    server: {
      baseDir: "./" + project + "/",
    },
    port: 3000,
    notify: false,
  });
}

function images() {
  //функция для работы с images
  return src(path.src.img, { encoding: false }) // путь к исходным файлам
    .pipe(
      webp({
        quality: 70,
      })
    )
    .pipe(dest(path.build.img))
    .pipe(src(path.src.img, { encoding: false }))
    .pipe(
      imagemin({
        progressive: true,
        svgoPlugins: [{ removeViewBox: false }],
        interlaced: true,
        optimizationLevel: 3, //0 to 7
      })
    )
    .pipe(dest(path.build.img)) //перебросить файлы в папку назначения дист
    .pipe(browsersync.stream()); //после всего браузер обновит страницу
}

function fonts() {
  src(path.src.fonts).pipe(ttf2woff2()).pipe(dest(path.build.fonts));
}

function html() {
  //функция для работы с HTML
  return (
    src(path.src.html) // путь к исходным файлам
      .pipe(fileinclude()) //обращаемся к переменной, чтобы плагин начал собирать файлы
      // .pipe(webphtml()) //подключаем плагин для обработки webp
      .pipe(dest(path.build.html)) //перебросить файлы в папку назначения дист
      .pipe(browsersync.stream())
  ); //после всего браузер обновит страницу
}

function js() {
  //функция для работы с js
  return src(path.src.js) // путь к исходным файлам
    .pipe(fileinclude()) //обращаемся к переменной, чтобы плагин начал собирать файлы
    .pipe(dest(path.build.js)) //перебросить файлы в папку назначения дист
    .pipe(uglify()) //после выгрузки незжатого файла будем сжимать js
    .pipe(
      rename({
        extname: ".min.js",
      })
    )
    .pipe(dest(path.build.js))
    .pipe(browsersync.stream()); //после всего браузер обновит страницу
}

function css() {
  //функция для работы с HTML
  return (
    src(path.src.css) // путь к исходным файлам
      .pipe(sass({ outputStyle: "expanded" }))
      .pipe(group_media())
      .pipe(
        autoprefixer({
          overrideBrowserslist: ["last 5 versions"],
          cascade: true,
        })
      )
      // .pipe(webpcss())
      .pipe(dest(path.build.css)) //выгрузка файла до сжатия и переименования
      .pipe(clean_css())
      .pipe(rename({ extname: ".min.css" }))
      .pipe(dest(path.build.css)) //перебросить файлы в папку назначения дист
      .pipe(browsersync.stream())
  ); //после всего браузер обновит страницу
}

function watchFiles(params) {
  //функция что-бы браузер обновлял страницу при нажатии save
  gulp.watch([path.watch.html], html);
  gulp.watch([path.watch.css], css);
  gulp.watch([path.watch.js], js);
  gulp.watch([path.watch.img], images);
}

function clean(params) {
  return del(path.clean);
}

let build = gulp.series(clean, gulp.parallel(js, css, html, images, fonts));
let watch = gulp.parallel(build, watchFiles, browserSync);

exports.fonts = fonts;
exports.images = images;
exports.js = js;
exports.css = css;
exports.html = html;
exports.build = build;
exports.watch = watch;
exports.default = watch;
