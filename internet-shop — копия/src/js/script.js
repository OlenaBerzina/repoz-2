const swiper = new Swiper(".swiper", {
  pagination: false,
  // grabCursor: true,
  on: {
    slideChange: function () {
      updatePagination(this.activeIndex);
    },
  },
});

const pagination = document.querySelectorAll(".swiper-pagination__custom span");
pagination.forEach((span, index) => {
  span.addEventListener("click", () => {
    swiper.slideTo(index);
    updatePagination(index);
  });
});
function updatePagination(activeIndex) {
  pagination.forEach((span) => {
    span.classList.remove("active");
  });
  pagination[activeIndex].classList.add("active");
}

document.querySelector(".body__header-call").onclick = function () {
  const menu = document.querySelector(".header-phone__list");
  if (menu.classList.contains("hidden")) {
    menu.classList.remove("hidden");
    menu.classList.add("visible");
  } else {
    menu.classList.remove("visible");
    menu.classList.add("hidden");
  }
};

document
  .querySelector(".body__header-call")
  .addEventListener("click", function () {
    this.classList.toggle("active"); // Переключаем класс "active" на кнопке
  });

document.querySelector(".menu__catalog-list").onclick = function (e) {
  e.preventDefault();

  const subMenu = document.querySelector(".sub-menu-catalog__block");

  subMenu.classList.toggle("active");
};

const burger = document.querySelector(".icon__menu");
const wrap_list = document.querySelector(".top__header-menu");
burger.addEventListener("click", function () {
  burger.classList.toggle("active");
  wrap_list.classList.toggle("active");
});

document.querySelector(".menu__item-btn").onclick = function () {
  const itemBurger = document.querySelector(".menu__item-burger");
  itemBurger.classList.toggle("visible");
  if (itemBurger.classList.contains("hidden")) {
    itemBurger.classList.remove("hidden");
    itemBurger.classList.add("visible");
  } else {
    itemBurger.classList.remove("visible");
    itemBurger.classList.add("hidden");
  }
};

document.querySelector(".menu__item-btn").onclick = function () {
  const burgerList = document.querySelector(".menu__item-burger");
  this.classList.toggle("active");
  if (burgerList.classList.contains("hidden")) {
    burgerList.classList.remove("hidden");
    burgerList.classList.add("visible");
  } else {
    burgerList.classList.remove("visible");
    burgerList.classList.add("hidden");
  }
};
